<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Mood;
use App\Entity\Person;
use App\Entity\Post;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use App\Entity\MoodChange;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class ReportLikeFilter extends AbstractContextAwareFilter
{
    private $entityManager;

    public function __construct(ManagerRegistry $managerRegistry, EntityManagerInterface $entityManager, ?RequestStack $requestStack = null, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null)
    {
        $this->entityManager = $entityManager;

        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $root = $queryBuilder->getRootAliases()[0];

        if (
            $resourceClass === Post::class &&
            $property === 'order' &&
            isset($value['reportsCount']) &&
            ($value['reportsCount'] === 'ASC' || $value['reportsCount'] === 'DESC')
        ) {
            // Reports Order
            $queryBuilder
                ->select($root . ', COUNT(r) AS HIDDEN nbReports')
                ->leftJoin($root . '.reports', 'r')
                ->addGroupBy($root)
                ->orderBy('nbReports', $value['reportsCount']);
        } else if (
            $resourceClass === Post::class &&
            $property === 'order' &&
            isset($value['likesCount']) &&
            ($value['likesCount'] === 'ASC' || $value['likesCount'] === 'DESC')
        ) {
            // Likes Order$queryBuilder
            $queryBuilder
                ->select($root . ', COUNT(l) AS HIDDEN nbLikes')
                ->leftJoin($root . '.likes', 'l')
                ->addGroupBy($root)
                ->orderBy('nbLikes', $value['likesCount']);
        } else {
            return;
        }
    }

    public function getDescription(string $resourceClass): array
    {
        return [];
    }
}
