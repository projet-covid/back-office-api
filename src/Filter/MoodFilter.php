<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Mood;
use App\Entity\Person;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use App\Entity\MoodChange;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

final class MoodFilter extends AbstractContextAwareFilter
{
    private $entityManager;

    public function __construct(ManagerRegistry $managerRegistry, EntityManagerInterface $entityManager, ?RequestStack $requestStack = null, LoggerInterface $logger = null, array $properties = null, NameConverterInterface $nameConverter = null)
    {
        $this->entityManager = $entityManager;

        parent::__construct($managerRegistry, $requestStack, $logger, $properties, $nameConverter);
    }

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if (
            !$this->isPropertyEnabled($property, $resourceClass) ||
            (!$this->isPropertyMapped($property, $resourceClass) && $property !== 'mood') ||
            $resourceClass !== Person::class ||
            $property !== 'mood'
        ) {
            return;
        }

        $root = $queryBuilder->getRootAliases()[0];

        $sql = "
            SELECT result.person_id
            FROM
            (SELECT p.id as person_id, MAX(mc.created_at) as maxcr
            FROM person p
            INNER JOIN mood_change mc ON mc.person_id = p.id
            GROUP BY p.id
            ORDER BY maxcr DESC) result
            INNER JOIN mood_change mmcc ON mmcc.person_id = result.person_id AND mmcc.created_at = result.maxcr AND mmcc.mood_id = :moodId
        ";

        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->bindValue('moodId', str_replace('/moods/', '', $value), \PDO::PARAM_INT);
        $stmt->execute();
        $result = array_map(function ($item) {
            return $item['person_id'];
        }, $stmt->fetchAll());

        $queryBuilder->andWhere(
            $queryBuilder->expr()->in($root . '.id', ':result')
        )->setParameter('result', $result);
    }

    public function getDescription(string $resourceClass): array
    {
        return [];
    }
}
