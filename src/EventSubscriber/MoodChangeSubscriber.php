<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\MoodChange;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class MoodChangeSubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['checkPersonAndCreatedAtValidity', EventPriorities::PRE_VALIDATE]
            ]
        ];
    }

    public function checkPersonAndCreatedAtValidity(ViewEvent $event)
    {
        /** @var MoodChange $moodChange */
        $moodChange = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$moodChange instanceof MoodChange || (Request::METHOD_POST !== $method && Request::METHOD_PUT !== $method)) {
            return;
        }

        $moodChange->setCreatedAt(new \DateTime());

        if ( $this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_USER') ) {
            /** @var Person $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $moodChange->setPerson($user);
        }
    }
}
