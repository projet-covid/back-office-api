<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Facility;
use App\Entity\Group;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class FacilitySubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['checkGroupValidity', EventPriorities::PRE_VALIDATE]
            ]
        ];
    }

    public function checkGroupValidity(ViewEvent $event)
    {
        /** @var Facility $facility */
        $facility = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$facility instanceof Facility || (Request::METHOD_POST !== $method && Request::METHOD_PUT !== $method)) {
            return;
        }

        if ( $this->security->isGranted('ROLE_ADMIN') ) {
            /** @var Group $groupToCheck */
            $adminGroup = $this->security->getUser()->getGroup();

            $facility->setGroup($adminGroup);
        }
    }
}
