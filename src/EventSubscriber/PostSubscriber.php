<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Person;
use App\Entity\Post;
use App\Repository\PostLikeRepository;
use App\Repository\PostReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

class PostSubscriber implements EventSubscriberInterface
{
    private $tokenStorage;
    private $entityManager;
    private $postLikeRepository;
    private $postReportRepository;
    private $storage;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        PostLikeRepository $postLikeRepository,
        PostReportRepository $postReportRepository,
        StorageInterface $storage
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->postLikeRepository = $postLikeRepository;
        $this->postReportRepository = $postReportRepository;
        $this->storage = $storage;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['setLikedAndReported', EventPriorities::PRE_WRITE],
                ['preValidatePostedPost', EventPriorities::PRE_VALIDATE]
            ]
        ];
    }

    /**
     * Sets a post liked and reported statuses.
     * @param ViewEvent $event
     * @throws Exception
     */
    public function setLikedAndReported(ViewEvent $event)
    {
        $data = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (
            !$data instanceof Post &&
            !(
                $data instanceof Paginator &&
                count($data) > 0 &&
                get_class($data->getIterator()->getArrayCopy()[0]) === Post::class
            ) ||
            Request::METHOD_GET !== $method
        ) {
            return;
        }

        /** @var Person $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        switch (get_class($data)) {
            case Post::class:
                $postLikes = $this->postLikeRepository->getPostLikesByPersonAndPost($currentUser, $data);
                $postReports = $this->postReportRepository->getPostReportsByPersonAndPost($currentUser, $data);

                $data->setLiked(count($postLikes) > 0);
                $data->setReported(count($postReports) > 0);
                if ( $data->getMedia() !== null ) {
                    $data->setMediaUrl($data->getMedia()->contentUrl);
                }
                break;
            case Paginator::class:
                /** @var Post $post */
                foreach ($data as $post) {
                    $postLikes = $this->postLikeRepository->getPostLikesByPersonAndPost($currentUser, $post);
                    $postReports = $this->postReportRepository->getPostReportsByPersonAndPost($currentUser, $post);

                    $post->setLiked(count($postLikes) > 0);
                    $post->setReported(count($postReports) > 0);
                    if ( $post->getMedia() !== null ) {
                        $post->setMediaUrl($this->storage->resolveUri($post->getMedia(), 'file'));
                    }
                }
                break;
        }
    }

    /**
     * @param ViewEvent $event
     */
    public function preValidatePostedPost(ViewEvent $event)
    {
        /** @var Post $post */
        $post = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ( !$post instanceof Post || Request::METHOD_POST !== $method ) {
            return;
        }

        /** @var Person $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $post->setPerson($user);

        foreach ( $post->getFacilities() as $facility ) {
            if ( is_null($user->getGroup()) || $facility->getGroup()->getId() != $user->getGroup()->getId() ) {
                $post->removeFacility($facility);
            }
        }
    }
}
