<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Validator\Exception\ValidationException;
use App\Entity\Facility;
use App\Entity\Group;
use App\Entity\MoodChange;
use App\Entity\Person;
use App\Repository\MoodChangeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;

class PersonSubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MoodChangeRepository
     */
    private $moodChangeRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var Security
     */
    private $security;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        MoodChangeRepository $moodChangeRepository,
        UserPasswordEncoderInterface $encoder,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->moodChangeRepository = $moodChangeRepository;
        $this->encoder = $encoder;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                ['checkIfGroupOwner', EventPriorities::PRE_WRITE],
                ['setMoodAndMoodSince', EventPriorities::PRE_WRITE],
                ['preValidatePostedPerson', EventPriorities::PRE_VALIDATE],
                ['preValidateUpdatedPerson', EventPriorities::PRE_VALIDATE]
            ]
        ];
    }

    /**
     * Check if a person owns a group before deleting it.
     * @param ViewEvent $event
     */
    public function checkIfGroupOwner(ViewEvent $event)
    {
        $person = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$person instanceof Person || Request::METHOD_DELETE !== $method) {
            return;
        }

        /** @var Person $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        if ( $person->getId() === $currentUser->getId() ) {
            throw new ValidationException(new ConstraintViolation(
                'Impossible de supprimer votre propre utilisateur.',
                null,
                array(),
                '',
                '',
                ''
            ));
        }

        /** @var Group[] */
        $ownedGroups = $this->entityManager->getRepository(Group::class)->findBy(['owner' => $person]);

        if ( count($ownedGroups) !== 0 ) {
            throw new ValidationException(new ConstraintViolation(
                'Impossible de supprimer un utilisateur qui est référent d\'un groupe.',
                null,
                array(),
                '',
                '',
                ''
            ));
        }
    }

    /**
     * Adds mood and since to persons before sending it to the view.
     * @param ViewEvent $event
     * @throws Exception
     */
    public function setMoodAndMoodSince(ViewEvent $event)
    {
        $data = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (
            !$data instanceof Person &&
            !(
                $data instanceof Paginator &&
                count($data) > 0 &&
                get_class($data->getIterator()->getArrayCopy()[0]) === Person::class
            ) ||
            Request::METHOD_GET !== $method
        ) {
            return;
        }

        /** @var Person $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        switch (get_class($data)) {
            case Person::class:
                if ( $data->getId() === $currentUser->getId() || $this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    /** @var MoodChange[] $moodChanges */
                    $moodChanges = $this->moodChangeRepository->getLastMoodChangeByPerson($data);
                    if ( count($moodChanges) === 1 ) {
                        $data->setMood($moodChanges[0]->getMood());
                        $data->setMoodSince($moodChanges[0]->getCreatedAt());
                    }
                }
                break;

            case Paginator::class:
                /** @var Person $person */
                foreach ($data as $person) {
                    if ( $person->getId() === $currentUser->getId() || $this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                        /** @var MoodChange[] $moodChanges */
                        $moodChanges = $this->moodChangeRepository->getLastMoodChangeByPerson($person);
                        if ( count($moodChanges) === 1 ) {
                            $person->setMood($moodChanges[0]->getMood());
                            $person->setMoodSince($moodChanges[0]->getCreatedAt());
                        }
                    }
                }
                break;
        }
    }

    /**
     * Pre Validates a posted person
     * @param ViewEvent $event
     */
    public function preValidatePostedPerson(ViewEvent $event)
    {
        $person = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$person instanceof Person || Request::METHOD_POST !== $method) {
            return;
        }

        // Password Check
        if ( $person->getPlainPassword() !== $person->getPlainPasswordConfirm() || empty($person->getPlainPassword()) ) {
            throw new ValidationException(new ConstraintViolation(
                'Le mot de passe et sa confirmation doivent être identiques et ne pas être vides.',
                null,
                array(),
                '',
                '',
                ''
            ));
        } else {
            $person->setPassword($this->encoder->encodePassword($person, $person->getPlainPassword()));
        }

        // Role Check
        if (
            $person->getRole() != null &&
            !$this->security->isGranted('ROLE_SUPER_ADMIN') &&
            $person->getRole()->getLabel() === 'ROLE_SUPER_ADMIN'
        ) {
            throw new ValidationException(new ConstraintViolation(
                'Rôle incorrect.',
                null,
                array(),
                '',
                '',
                ''
            ));
        }

        // Group Check
        if (
            !$this->security->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $person->setGroup($this->security->getUser()->getGroup());
        }

        // Empty Facilities
        foreach ( $person->getFacilities() as $facility ) {
            $person->removeFacility($facility);
        }
    }

    /**
     * Pre Validates an updated person
     * @param ViewEvent $event
     */
    public function preValidateUpdatedPerson(ViewEvent $event)
    {
        /** @var Person $person */
        $person = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$person instanceof Person || Request::METHOD_PUT !== $method) {
            return;
        }

        // Password Check
        if ( $person->getPlainPassword() === $person->getPlainPasswordConfirm() && !empty($person->getPlainPassword()) ) {
            $person->setPassword($this->encoder->encodePassword($person, $person->getPlainPassword()));
           if ( $person->getLastLogged() === null ) {
               $person->setLastLogged(new \DateTime());
           }
        }

        // Role Check
        if (
            $person->getRole() != null &&
            !$this->security->isGranted('ROLE_SUPER_ADMIN') &&
            $person->getRole()->getLabel() === 'ROLE_SUPER_ADMIN'
        ) {
            throw new ValidationException(new ConstraintViolation(
                'Rôle incorrect.',
                null,
                array(),
                '',
                '',
                ''
            ));
        }

        // Group Check
        if (
        !$this->security->isGranted('ROLE_SUPER_ADMIN')
        ) {
            $person->setGroup($this->security->getUser()->getGroup());
        }

        // Facilities Check
        /** @var Facility $facility */
        if ( count($person->getFacilities()) > 0 ) {
            foreach ( $person->getFacilities() as $facility) {
                if ( $facility->getGroup()->getId() !== $person->getGroup()->getId() ) {
                    $person->removeFacility($facility);
                }
            }
        }
    }
}
