<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Facility;
use App\Entity\Group;
use App\Entity\Mood;
use App\Entity\MoodChange;
use App\Entity\Person;
use App\Entity\Post;
use App\Entity\PostLike;
use App\Entity\PostReport;
use App\Entity\Role;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;

final class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = [])
    {
        $this->addWhere($queryBuilder, $resourceClass);
    }

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
    {
        $rootAlias = $queryBuilder->getRootAliases()[0];

        /** @var Person $user */
        $user = $this->security->getUser();

        switch ($resourceClass) {
            case Facility::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->andWhere(sprintf('%s.group = :group', $rootAlias));
                        $queryBuilder->setParameter('group', $user->getGroup());
                        $queryBuilder->innerJoin(sprintf('%s.group', $rootAlias), 'fg');
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->orX(
                                $queryBuilder->expr()->eq('fg.owner', ':user'),
                                $queryBuilder->expr()->in(sprintf('%s', $rootAlias), ':user_facilities')
                            )
                        );
                        $queryBuilder->setParameter('user', $user->getId());
                        $queryBuilder->setParameter('user_facilities', $user->getFacilities());
                    } else {
                        $queryBuilder->innerJoin(sprintf('%s.persons', $rootAlias), 'fpe');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('fpe.id', ':user'));
                        $queryBuilder->andWhere(sprintf('%s.group = :group', $rootAlias));
                        $queryBuilder->setParameter('user', $user->getId());
                        $queryBuilder->setParameter('group', $user->getGroup());
                    }
                }
                break;
            case Group::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    $queryBuilder->andWhere(sprintf('%s = :group', $rootAlias));
                    $queryBuilder->setParameter('group', $user->getGroup());
                }
                break;
            case Mood::class:
                break;
            case MoodChange::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->innerJoin(sprintf('%s.person', $rootAlias), 'mcpe');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('mcpe.group', ':group'));
                        $queryBuilder->setParameter(':group', $user->getGroup());
                    } else {
                        $queryBuilder->andWhere($queryBuilder->expr()->eq(sprintf('%s.person', $rootAlias), ':user'));
                        $queryBuilder->setParameter(':user', $user);
                    }
                }
                break;
            case Person::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->andWhere($queryBuilder->expr()->eq(sprintf('%s.group', $rootAlias), ':group'));
                        $queryBuilder->setParameter('group', $user->getGroup());
                        $queryBuilder->innerJoin(sprintf('%s.group', $rootAlias), 'pg');
                        $queryBuilder->leftJoin(sprintf('%s.facilities', $rootAlias), 'pf');
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->orX(
                                $queryBuilder->expr()->orX(
                                    $queryBuilder->expr()->eq('pg.owner', ':user'),
                                    $queryBuilder->expr()->in('pf', ':user_facilities')
                                ),
                                $queryBuilder->expr()->eq(sprintf('%s.id', $rootAlias), ':person_id')
                            )
                        );
                        $queryBuilder->setParameter('user', $user->getId());
                        $queryBuilder->setParameter('user_facilities', $user->getFacilities());
                        $queryBuilder->setParameter('person_id', $user->getId());
                    } else {
                        $queryBuilder->andWhere($queryBuilder->expr()->eq(sprintf('%s.id', $rootAlias), ':person_id'));
                        $queryBuilder->setParameter('person_id', $user->getId());
                    }
                }
                break;
            case Post::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->innerJoin(sprintf('%s.facilities', $rootAlias), 'pf');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('pf.group', ':group'));
                        $queryBuilder->setParameter('group', $user->getGroup());
                        $queryBuilder->innerJoin('pf.group', 'pfg');
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->orX(
                                $queryBuilder->expr()->eq('pfg.owner', ':user'),
                                $queryBuilder->expr()->in('pf', ':user_facilities')
                            )
                        );
                        $queryBuilder->setParameter('user', $user->getId());
                        $queryBuilder->setParameter('user_facilities', $user->getFacilities());
                    } else {
                        $queryBuilder->innerJoin(sprintf('%s.facilities', $rootAlias), 'pf');
                        $queryBuilder->andWhere($queryBuilder->expr()->in('pf', ':facilities'));
                        $queryBuilder->setParameter('facilities', $user->getFacilities());
                    }
                }
                break;
            case PostLike::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->innerJoin(sprintf('%s.post', $rootAlias), 'plp');
                        $queryBuilder->innerJoin('plp.facilities', 'plpf');
                        $queryBuilder->innerJoin('plpf.group', 'plpfg');
                        $queryBuilder->innerJoin(sprintf('%s.person', $rootAlias), 'plpe');
                        $queryBuilder->innerJoin('plpe.group', 'plpeg');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('plpfg', ':group'));
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('plpeg', ':group'));
                        $queryBuilder->setParameter('group', $user->getGroup());
                    } else {
                        $queryBuilder->innerJoin(sprintf('%s.post', $rootAlias), 'plp');
                        $queryBuilder->innerJoin('plp.person', 'plppe');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('plppe', ':user'));
                        $queryBuilder->setParameter('user', $user);
                    }
                }
                break;
            case PostReport::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->innerJoin(sprintf('%s.post', $rootAlias), 'prp');
                        $queryBuilder->innerJoin('prp.facilities', 'prpf');
                        $queryBuilder->innerJoin('prpf.group', 'prpfg');
                        $queryBuilder->innerJoin(sprintf('%s.person', $rootAlias), 'prpe');
                        $queryBuilder->innerJoin('prpe.group', 'prpeg');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('prpfg', ':group'));
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('prpeg', ':group'));
                        $queryBuilder->setParameter('group', $user->getGroup());
                    } else {
                        $queryBuilder->innerJoin(sprintf('%s.post', $rootAlias), 'prp');
                        $queryBuilder->innerJoin('prp.person', 'prppe');
                        $queryBuilder->andWhere($queryBuilder->expr()->eq('prppe', ':user'));
                        $queryBuilder->setParameter('user', $user);
                    }
                }
                break;
            case Role::class:
                if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
                    if ( $this->security->isGranted('ROLE_ADMIN') ) {
                        $queryBuilder->andWhere(
                            $queryBuilder->expr()->orX(
                                $queryBuilder->expr()->eq(sprintf('%s.label', $rootAlias), ':ROLE_ADMIN'),
                                $queryBuilder->expr()->eq(sprintf('%s.label', $rootAlias), ':ROLE_USER')
                            )
                        );
                        $queryBuilder->setParameter('ROLE_ADMIN', 'ROLE_ADMIN');
                        $queryBuilder->setParameter('ROLE_USER', 'ROLE_USER');
                    } else {
                        $queryBuilder->andWhere($queryBuilder->expr()->eq(sprintf('%s.label', $rootAlias), ':ROLE_USER'));
                        $queryBuilder->setParameter('ROLE_USER', 'ROLE_USER');
                    }
                }
                break;
            default:
                return;
        }

        return;
    }
}
