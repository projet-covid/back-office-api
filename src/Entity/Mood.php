<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * A mood.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"mood"}},
 *     collectionOperations={
 *         "get",
 *         "post"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN')",
 *             "validation_groups"={"postValidation"}
 *         }
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN')",
 *             "validation_groups"={"putValidation"}
 *         },
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ORM\Entity
 * @ApiFilter(OrderFilter::class, properties={"label"}, arguments={"orderParameterName"="order"})
 */
class Mood
{
    /**
     * @var int The mood Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "mood", "mood_change", "person"})
     */
    private $id;

    /**
     * @var string The mood Label
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"postValidation", "putValidation"})
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"post", "mood", "mood_change", "person"})
     */
    private $label;

    /**
     * @var string The mood Icon
     *
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"postValidation", "putValidation"})
     * @Assert\NotNull(groups={"postValidation", "putValidation"})
     * @Groups({"post", "mood", "mood_change", "person"})
     */
    private $icon;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Mood
     */
    public function setLabel(string $label): Mood
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     * @return Mood
     */
    public function setIcon(string $icon): Mood
    {
        $this->icon = $icon;
        return $this;
    }
}
