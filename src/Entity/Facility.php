<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use App\Validator\AdminGroupsGenerator;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * A facility.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"facility"}},
 *     collectionOperations={
 *         "get",
 *         "post"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')",
 *             "validation_groups"=AdminGroupsGenerator::class
 *         }
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN') && user.getGroup() == object.getGroup()",
 *             "validation_groups"=AdminGroupsGenerator::class
 *         },
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN') or (is_granted('ROLE_ADMIN') and object.getGroup() == user.getGroup())"}
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"label"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"persons": "partial", "posts": "partial", "label": "partial"})
 * @ORM\Entity
 */
class Facility
{
    /**
     * @var int The facility Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "facility", "person"})
     */
    private $id;

    /**
     * @var string The facility Label
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"postValidationSuperAdmin", "postValidationAdmin", "putValidationSuperAdmin", "putValidationAdmin"})
     * @Assert\NotNull(groups={"postValidationSuperAdmin", "postValidationAdmin", "putValidationSuperAdmin", "putValidationAdmin"})
     * @Assert\Length(min="3", max="255", groups={"postValidationSuperAdmin", "postValidationAdmin", "putValidationSuperAdmin", "putValidationAdmin"})
     * @Groups({"post", "facility", "person"})
     */
    private $label;

    /**
     * @var Group The facility Group
     *
     * @ORM\ManyToOne(targetEntity="Group")
     * @Assert\NotBlank(groups={"postValidationSuperAdmin", "putValidationSuperAdmin"})
     * @Assert\NotNull()
     * @Groups("facility")
     */
    private $group;

    /**
     * @var Collection The facility Persons
     *
     * @ORM\ManyToMany(targetEntity="Person", mappedBy="facilities")
     * @Groups("facility")
     */
    private $persons;

    /**
     * @var Collection The facility posts
     *
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="facilities")
     */
    private $posts;

    /**
     * Facility constructor.
     */
    public function __construct()
    {
        $this->persons = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Facility
     */
    public function setLabel(string $label): Facility
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup(): ?Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return Facility
     */
    public function setGroup(?Group $group): Facility
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    /**
     * @param Person $person
     * @return Facility
     */
    public function addPerson(Person $person): Facility
    {
        $this->persons->add($person);
    }

    /**
     * @param Person $person
     * @return Facility
     */
    public function removePerson(Person $person): Facility
    {
        $this->persons->removeElement($person);
    }
}
