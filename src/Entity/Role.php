<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A role.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"role"}},
 *     collectionOperations={
 *         "get"
 *     },
 *     itemOperations={
 *         "get"
 *     }
 * )
 * @ORM\Entity
 */
class Role
{
    /**
     * @var int The Role Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"person", "role"})
     */
    private $id;

    /**
     * @var string The Role Label
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"person", "role"})
     */
    private $label;

    /**
     * @var string The Role Displayed Label
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"person", "role"})
     */
    private $displayedLabel;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $label
     * @return Role
     */
    public function setLabel(string $label): Role
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getDisplayedLabel(): ?string
    {
        return $this->displayedLabel;
    }

    /**
     * @param string $displayedLabel
     * @return Role
     */
    public function setDisplayedLabel(?string $displayedLabel): Role
    {
        $this->displayedLabel = $displayedLabel;
        return $this;
    }
}
