<?php

namespace App\Entity;

use App\Validator\AdminGroupsGenerator;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A mood change.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"mood_change"}},
 *     collectionOperations={
 *         "get",
 *         "get_mood_changes_by_person"={"route_name"="get_mood_changes_by_person"},
 *         "post"={"validation_groups"=AdminGroupsGenerator::class}
 *     },
 *     itemOperations={
 *         "get",
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ORM\Entity
 */
class MoodChange
{
    /**
     * @var int The mood change Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("mood_change")
     */
    private $id;

    /**
     * @var DateTime The MoodChange Created At date and time
     *
     * @ORM\Column(type="datetime")
     * @Groups("mood_change")
     */
    private $createdAt;

    /**
     * @var Mood The MoodChange Mood
     *
     * @ORM\ManyToOne(targetEntity="Mood")
     * @Assert\NotBlank(groups={"Default", "postValidation"})
     * @Assert\NotNull(groups={"Default", "postValidation"})
     * @Groups("mood_change")
     */
    private $mood;

    /**
     * @var Person The MoodChange Person
     *
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="moodChanges")
     * @Assert\NotBlank(groups={"postValidation"})
     * @Assert\NotNull(groups={"postValidation"})
     * @Groups("mood_change")
     */
    private $person;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return MoodChange
     */
    public function setCreatedAt(?DateTime $createdAt): MoodChange
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return Mood
     */
    public function getMood(): ?Mood
    {
        return $this->mood;
    }

    /**
     * @param Mood $mood
     * @return MoodChange
     */
    public function setMood(?Mood $mood): MoodChange
    {
        $this->mood = $mood;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return MoodChange
     */
    public function setPerson(?Person $person): MoodChange
    {
        $this->person = $person;
        return $this;
    }
}
