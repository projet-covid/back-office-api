<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Filter\MoodFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * A person.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"person"}},
 *     denormalizationContext={"groups"={"personPost", "personPut"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN') or object == user"},
 *         "put"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN') or (is_granted('ROLE_ADMIN') and object.getGroup() == user.getGroup())"}
 *     }
 * )
 * @ApiFilter(MoodFilter::class)
 * @ApiFilter(OrderFilter::class, properties={"lastName", "firstName", "username"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"facilities": "partial", "username": "partial"})
 * @ORM\Entity
 */
class Person implements UserInterface
{
    /**
     * @var int The Person Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report"})
     */
    private $id;

    /**
     * @var string The Person Last Name
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report", "personPost", "personPut"})
     */
    private $lastName;

    /**
     * @var string The Person First Name
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report", "personPost", "personPut"})
     */
    private $firstName;

    /**
     * @var string The Person Username
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="255")
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report", "personPost", "personPut"})
     */
    private $username;

    /**
     * @var string The Person Password
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string The Person Plain Password
     *
     * @Groups({"personPost", "personPut"})
     */
    private $plainPassword;

    /**
     * @var string The Person Plain Password Confirm
     *
     * @Groups({"personPost", "personPut"})
     */
    private $plainPasswordConfirm;

    /**
     * @var DateTime The Person Last Logged date and time
     *
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report"})
     */
    private $lastLogged;

    /**
     * @var boolean The Person Archived Status
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report", "personPut"})
     */
    private $archived;

    /**
     * @var boolean The Person Show Mood Status
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"post", "facility", "group", "mood_change", "person", "post_like", "post_report"})
     */
    private $showMood;

    /**
     * @var Role The Person Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups({"person", "personPost", "personPut"})
     */
    private $role;

    /**
     * @var Group The Person Group
     *
     * @ORM\ManyToOne(targetEntity="Group")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups({"person", "personPost", "personPut"})
     */
    private $group;

    /**
     * @var Collection The Person Facilities
     *
     * @ORM\ManyToMany(targetEntity="Facility", inversedBy="persons")
     * @Groups({"person", "personPut"})
     */
    private $facilities;

    /**
     * @var Mood The Person Actual Mood
     * @Groups("person")
     */
    private $mood;

    /**
     * @var DateTime The Person Actual Mood Since Date and Time
     * @Groups("person")
     */
    private $moodSince;

    /**
     * @var Collection The Person MoodChanges
     * @ORM\OneToMany(targetEntity="MoodChange", mappedBy="person")
     */
    private $moodChanges;

    /**
     * Person constructor.
     */
    public function __construct()
    {
        $this->archived = false;
        $this->showMood = true;

        $this->facilities = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Person
     */
    public function setLastName(string $lastName): Person
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Person
     */
    public function setFirstName(string $firstName): Person
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Person
     */
    public function setUsername(string $username): Person
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Person
     */
    public function setPassword(string $password): Person
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return Person
     */
    public function setPlainPassword(string $plainPassword): Person
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPasswordConfirm(): ?string
    {
        return $this->plainPasswordConfirm;
    }

    /**
     * @param string $plainPasswordConfirm
     * @return Person
     */
    public function setPlainPasswordConfirm(string $plainPasswordConfirm): Person
    {
        $this->plainPasswordConfirm = $plainPasswordConfirm;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getLastLogged(): ?DateTime
    {
        return $this->lastLogged;
    }

    /**
     * @param DateTime $lastLogged
     * @return Person
     */
    public function setLastLogged(?DateTime $lastLogged): Person
    {
        $this->lastLogged = $lastLogged;
        return $this;
    }

    /**
     * @return bool
     */
    public function isArchived(): ?bool
    {
        return $this->archived;
    }

    /**
     * @param bool $archived
     * @return Person
     */
    public function setArchived(?bool $archived): Person
    {
        $this->archived = $archived ? true : false;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowMood(): ?bool
    {
        return $this->showMood;
    }

    /**
     * @param bool $showMood
     * @return Person
     */
    public function setShowMood(?bool $showMood): Person
    {
        $this->showMood = $showMood ? true : false;
        return $this;
    }

    /**
     * @return Role
     */
    public function getRole(): ?Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return Person
     */
    public function setRole(Role $role): Person
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return Group
     */
    public function getGroup(): ?Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     * @return Person
     */
    public function setGroup(?Group $group): Person
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFacilities(): Collection
    {
        return $this->facilities;
    }

    /**
     * @param Facility $facility
     * @return Person
     */
    public function addFacility(Facility $facility): Person
    {
        $this->facilities->add($facility);
        return $this;
    }

    /**
     * @param Facility $facility
     * @return Person
     */
    public function removeFacility(Facility $facility): Person
    {
        $this->facilities->removeElement($facility);
        return $this;
    }

    /**
     * @return Mood
     */
    public function getMood(): ?Mood
    {
        return $this->mood;
    }

    /**
     * @param Mood $mood
     * @return Person
     */
    public function setMood(?Mood $mood): Person
    {
        $this->mood = $mood;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getMoodSince(): ?DateTime
    {
        return $this->moodSince;
    }

    /**
     * @param DateTime $moodSince
     * @return Person
     */
    public function setMoodSince(?DateTime $moodSince): Person
    {
        $this->moodSince = $moodSince;
        return $this;
    }

    public function getRoles()
    {
        return [$this->getRole()->getLabel()];
    }

    public function getSalt()
    {
        return;
    }

    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
