<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A post report.
 *
 * @ApiResource(
 *     attributes={ "input_formats"={"json"={"application/ld+json", "application/json"}}, "output_formats"={"json"={"application/ld+json", "application/json"}} },
 *     normalizationContext={"groups"={"post_report"}},
 *     collectionOperations={
 *         "get",
 *         "get_post_reports_by_post"={"route_name"="get_post_reports_by_post"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') ans object.post.getFacility().getGroup() == user.getGroup())"},
 *         "add_or_remove_post_report"={"route_name"="add_or_remove_post_report"}
 *     }
 * )
 * @ORM\Entity
 */
class PostReport
{
    /**
     * @var int The PostReport Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "post_report"})
     */
    private $id;

    /**
     * @var Person The PostReport Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("post_report")
     */
    private $person;

    /**
     * @var Post The PostReport Post
     *
     * @ORM\ManyToOne(targetEntity="Post")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("post_report")
     */
    private $post;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Person
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return PostReport
     */
    public function setPerson(?Person $person): PostReport
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Post
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return PostReport
     */
    public function setPost(?Post $post): PostReport
    {
        $this->post = $post;
        return $this;
    }
}
