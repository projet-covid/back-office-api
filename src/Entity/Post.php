<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\ReportLikeFilter;

/**
 * A post.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"post"}},
 *     denormalizationContext={"groups"={"postValidate", "putValidate"}},
 *     collectionOperations={
 *         "get",
 *         "get_posts_by_facility"={
 *              "route_name"="get_posts_by_facility",
 *              "pagination_fetch_join_collection"=true
 *         },
 *         "post",
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ORM\Entity
 * @ApiFilter(ReportLikeFilter::class)
 * @ApiFilter(OrderFilter::class, properties={"publishedAt"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"facilities": "partial"})
 */
class Post
{
    #region Base Properties

    /**
     * @var int The Post Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "post_like", "post_report"})
     */
    private $id;

    /**
     * @var string The Post Content
     *
     * @ORM\Column(type="text", length=280)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(min="3", max="280")
     * @Groups({"post", "post_like", "post_report", "postValidate"})
     */
    private $content;

    /**
     * @var boolean The Post Archived Status
     *
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull()
     * @Groups({"post", "post_like", "post_report", "putValidate"})
     */
    private $archived;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     * @Groups("post")
     */
    private $publishedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Post
     */
    public function setId(int $id): Post
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Post
     */
    public function setContent(string $content): Post
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->archived;
    }

    /**
     * @param bool $archived
     * @return Post
     */
    public function setArchived(bool $archived): Post
    {
        $this->archived = $archived;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt(): ?\DateTime
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     * @return Post
     * @throws \Exception
     * @Groups("post")
     */
    public function setPublishedAt(?\DateTime $publishedAt): Post
    {
        $this->publishedAt = $publishedAt ? $publishedAt : new \DateTime('now');
        return $this;
    }

    #endregion

    #region Nested Properties

    /**
     * @var Mood The Post Mood
     *
     * @ORM\ManyToOne(targetEntity="Mood")
     * @Groups({"post", "postValidate"})
     */
    private $mood;

    /**
     * @var Person The Post Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Groups("post")
     */
    private $person;

    /**
     * @var Collection The Post Facilities
     *
     * @ORM\ManyToMany(targetEntity="Facility", inversedBy="posts")
     * @Assert\Count(min="1")
     * @Groups({"post", "postValidate"})
     */
    private $facilities;

    /**
     * @var Collection The Post Likes
     *
     * @ORM\OneToMany(targetEntity="PostLike", mappedBy="post")
     * @Groups("post")
     */
    private $likes;

    /**
     * @var Collection The Post Reports
     *
     * @ORM\OneToMany(targetEntity="PostReport", mappedBy="post")
     * @Groups("post")
     */
    private $reports;

    /**
     * @var Media|null
     *
     * @ORM\ManyToOne(targetEntity=Media::class)
     * @ORM\JoinColumn(nullable=true)
     */
    public $media;

    /**
     * @return Mood
     */
    public function getMood(): ?Mood
    {
        return $this->mood;
    }

    /**
     * @param Mood $mood
     * @return Post
     */
    public function setMood(?Mood $mood): Post
    {
        $this->mood = $mood;
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Post
     */
    public function setPerson(Person $person): Post
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFacilities(): Collection
    {
        return $this->facilities;
    }

    /**
     * @param Facility $facility
     * @return Post
     */
    public function addFacility(Facility $facility): Post
    {
        $this->facilities->add($facility);
        return $this;
    }

    /**
     * @param Facility $facility
     * @return Post
     */
    public function removeFacility(Facility $facility): Post
    {
        $this->facilities->removeElement($facility);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getLikes(): Collection
    {
        return $this->likes;
    }

    /**
     * @param PostLike $like
     * @return Post
     */
    public function addLike(PostLike $like): Post
    {
        $this->likes->add($like);
        return $this;
    }

    /**
     * @param PostLike $like
     * @return Post
     */
    public function removeLike(PostLike $like): Post
    {
        $this->likes->removeElement($like);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    /**
     * @param PostReport $report
     * @return Post
     */
    public function addReport(PostReport $report): Post
    {
        $this->reports->add($report);
        return $this;
    }

    /**
     * @param PostReport $report
     * @return Post
     */
    public function removeReport(PostReport $report): Post
    {
        $this->reports->removeElement($report);
        return $this;
    }

    /**
     * @return Media|null
     */
    public function getMedia(): ?Media
    {
        return $this->media;
    }

    /**
     * @param Media|null $image
     * @return Post
     */
    public function setMedia(?Media $image): Post
    {
        $this->media = $image;
        return $this;
    }

    #endregion

    #region Added Properties

    /**
     * @var boolean The Post Liked by Current User Status
     * @Groups("post")
     */
    private $liked;

    /**
     * @var boolean The post Reported by Current User Status
     * @Groups("post")
     */
    private $reported;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"post", "postValidate"})
     */
    private $mediaUrl;

    /**
     * @return bool
     */
    public function isLiked(): ?bool
    {
        return $this->liked;
    }

    /**
     * @param bool $liked
     * @return Post
     */
    public function setLiked(?bool $liked): Post
    {
        $this->liked = $liked;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReported(): ?bool
    {
        return $this->reported;
    }

    /**
     * @param bool $reported
     * @return Post
     */
    public function setReported(?bool $reported): Post
    {
        $this->reported = $reported;
        return $this;
    }

    /**
     * @return string
     */
    public function getMediaUrl(): ?string
    {
        return $this->mediaUrl;
    }

    /**
     * @param string $mediaUrl
     * @return Post
     */
    public function setMediaUrl(?string $mediaUrl): Post
    {
        $this->mediaUrl = $mediaUrl;
        return $this;
    }

    #endregion

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->archived = false;
        $this->liked = false;
        $this->reported = false;

        $this->publishedAt = new \DateTime('now');

        $this->facilities = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }
}
