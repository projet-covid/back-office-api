<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Person;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * A group.
 *
 * @ApiResource(
 *     normalizationContext={"groups"={"group"}},
 *     collectionOperations={
 *         "get",
 *         "post"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN')",
 *             "validation_groups"={"postValidation"}
 *         }
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={
 *             "security"="is_granted('ROLE_SUPER_ADMIN')",
 *             "validation_groups"={"putValidation"}
 *         },
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"}
 *     }
 * )
 * @ORM\Entity
 * @ORM\Table(name="grouping")
 * @ApiFilter(OrderFilter::class, properties={"label"}, arguments={"orderParameterName"="order"})
 */
class Group
{
    /**
     * @var int The group Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"facility", "group", "person"})
     */
    private $id;

    /**
     * @var string The group Label
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"postValidation", "putValidation"})
     * @Assert\NotNull(groups={"postValidation", "putValidation"})
     * @Assert\Length(min="3", max="255", groups={"postValidation", "putValidation"})
     * @Groups({"facility", "group", "person"})
     */
    private $label;

    /**
     * @var Person The group Owner
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @Assert\NotBlank(groups={"putValidation"})
     * @Assert\NotNull(groups={"putValidation"})
     * @Groups("group")
     */
    private $owner;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return Group
     */
    public function setLabel(string $label): Group
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return Person
     */
    public function getOwner(): ?Person
    {
        return $this->owner;
    }

    /**
     * @param Person $owner
     * @return Group
     */
    public function setOwner(?Person $owner): Group
    {
        $this->owner = $owner;
        return $this;
    }
}
