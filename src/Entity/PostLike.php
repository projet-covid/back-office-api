<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A post like.
 *
 * @ApiResource(
 *     attributes={ "input_formats"={"json"={"application/ld+json", "application/json"}}, "output_formats"={"json"={"application/ld+json", "application/json"}} },
 *     normalizationContext={"groups"={"post_like"}},
 *     collectionOperations={
 *         "get",
 *         "get_post_likes_by_post"={"route_name"="get_post_likes_by_post"}
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_ADMIN') or (is_granted('ROLE_USER') ans object.post.getFacility().getGroup() == user.getGroup())"},
 *         "add_or_remove_post_like"={"route_name"="add_or_remove_post_like"}
 *     }
 * )
 * @ORM\Entity
 */
class PostLike
{
    /**
     * @var int The PostLike Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post", "post_like"})
     */
    private $id;

    /**
     * @var Person The PostLike Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("post_like")
     */
    private $person;

    /**
     * @var Post The PostLike Post
     *
     * @ORM\ManyToOne(targetEntity="Post")
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("post_like")
     */
    private $post;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Person
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return PostLike
     */
    public function setPerson(?Person $person): PostLike
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Post
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return PostLike
     */
    public function setPost(?Post $post): PostLike
    {
        $this->post = $post;
        return $this;
    }
}
