<?php

namespace App\Controller;

use DateTime;
use App\Entity\MoodChange;
use App\Entity\Person;
use App\Entity\Mood;
use App\Repository\MoodChangeRepository;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\QueryException;

class MoodChangeController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MoodChangeRepository
     */
    private $moodChangesRepository;

    /**
     * PostController constructor.
     * @param EntityManagerInterface $entityManager
     * @param MoodChangeRepository $moodChangesRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        MoodChangeRepository $moodChangesRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->moodChangesRepository = $moodChangesRepository;
    }

    /**
     * @param Request $request
     * @param int $personId
     * @return MoodChange[]|array
     * @throws QueryException
     */
    public function getByPerson(Request $request, int $personId)
    {
        $page = (int) $request->query->get('page', 1);

        /** @var Person $person */
        $person = $this->entityManager
            ->getRepository(Person::class)
            ->find($personId);

        if ( !is_null($person) ) {
            /** @var MoodChange[] $moodChanges */
            $moodChanges = $this->moodChangesRepository
                ->getMoodChangesByPerson($person, $page);

            return $moodChanges;
        } else {
            return [];
        }
    }

    /**
     * @param Request $request
     * @return boolean
     */
    public function postByPerson(Request $request)
    {
        $personId = $request->request->has('person_id');
        $moodId = $request->request->has('mood_id');

        /** @var Person $person */
        $person = $this->entityManager
            ->getRepository(Person::class)
            ->find($personId);

        /** @var Person $person */
        $mood = $this->entityManager
            ->getRepository(Mood::class)
            ->find($moodId);

        if ( !is_null($person) && !is_null($mood) ) {
            $moodChange = new MoodChange();
            $moodChange->setCreatedAt(new DateTime);
            $moodChange->setMood($mood);
            $moodChange->setPerson($person);
            
            $this->entityManager->persist($moodChange);
            $this->entityManager->flush();

            return true;
        } else {
            return false;
        }
    }
}
