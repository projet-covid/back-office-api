<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Post;
use App\Entity\PostLike;
use App\Repository\PostLikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PostLikeController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PostLikeRepository
     */
    private $postLikeRepository;

    /**
     * PostLikeController constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     * @param PostLikeRepository $postLikeRepository
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        PostLikeRepository $postLikeRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->postLikeRepository = $postLikeRepository;
    }

    /**
     * @param $postId
     * @return PostLike[]
     */
    public function getByPost($postId)
    {
        /** @var Post $post */
        $post = $this->entityManager
            ->getRepository(Post::class)
            ->find($postId);

        if ( !is_null($post) ) {
            /** @var PostLike[] $posts */
            $likes = $this->postLikeRepository
                ->getPostLikesByPost($post);

            return $likes;
        } else {
            return [];
        }
    }

    /**
     * @param Request $request
     * @param $postId
     * @return array
     */
    public function addOrRemoveLike(Request $request, $postId)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());

        /** @var Post $post */
        $post = $this->entityManager
            ->getRepository(Post::class)
            ->find($postId);

        $wantsToLike = (bool) $request->get('like');

        if ( !is_null($post) ) {
            /** @var Person $person */
            $person = $this->tokenStorage->getToken()->getUser();

            $likes = $this->postLikeRepository->getPostLikesByPersonAndPost($person, $post);

            if ( $wantsToLike ) {
                if ( count($likes) <= 0 ) {
                    $newLike = new PostLike();
                    $newLike
                        ->setPost($post)
                        ->setPerson($person);
                    $this->entityManager->persist($newLike);
                    $this->entityManager->flush();
                }
            } else {
                if ( count($likes) > 0 ) {
                    /** @var PostLike $oldLike */
                    foreach ( $likes as $oldLike ) {
                        $this->entityManager->remove($oldLike);
                    }
                    $this->entityManager->flush();
                }
            }

            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}
