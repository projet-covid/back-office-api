<?php

namespace App\Controller;

use ApiPlatform\Core\Validator\Exception\ValidationException;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\ConstraintViolation;

class PersonController extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * PersonController constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        UserPasswordEncoderInterface $encoder,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager
    )
    {
        $this->encoder = $encoder;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    public function changePassword(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());

        $currentPassword = $request->get('currentPassword');
        $newPassword = $request->get('newPassword');
        $newPasswordConfirm = $request->get('newPasswordConfirm');

        if ( empty($currentPassword) || empty($newPassword) || empty($newPasswordConfirm) ) {
            throw new ValidationException(new ConstraintViolation(
                'Aucun champ ne doit être vide.',
                null,
                array(),
                '',
                '',
                ''
            ));
        } else {
            /** @var Person $user */
            $user = $this->tokenStorage->getToken()->getUser();
            if (!$this->encoder->isPasswordValid($user, $currentPassword)) {
                throw new ValidationException(new ConstraintViolation(
                    'Le mot de passe actuel est incorrect.',
                    null,
                    array(),
                    '',
                    '',
                    ''
                ));
            } else {
                if ( $newPassword !== $newPasswordConfirm ) {
                    throw new ValidationException(new ConstraintViolation(
                        'Le nouveau mot de passe n\'est pas identique à la confirmation.',
                        null,
                        array(),
                        '',
                        '',
                        ''
                    ));
                } else {
                    $encoded = $this->encoder->encodePassword($user, $newPassword);
                    $user->setPassword($encoded);
                    $user->setLastLogged(new \DateTime());
                    $this->entityManager->persist($user);
                    $this->entityManager->flush();
                }
            }
        }

        return new JsonResponse(['success' => true]);
    }
}
