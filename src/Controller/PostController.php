<?php

namespace App\Controller;

use App\Entity\Facility;
use App\Entity\Post;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\QueryException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param EntityManagerInterface $entityManager
     * @param PostRepository $postRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        PostRepository $postRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->postRepository = $postRepository;
    }

    /**
     * @param int $facilityId
     * @param Request $request
     * @return object[]
     * @throws QueryException
     */
    public function getByFacility(Request $request, int $facilityId)
    {
        $page = (int) $request->query->get('page', 1);

        /** @var Facility $facility */
        $facility = $this->entityManager
            ->getRepository(Facility::class)
            ->find($facilityId);

        if ( !is_null($facility) ) {
            /** @var Post[] $posts */
            $posts = $this->postRepository
                ->getPostsByFacility($facility, $page);

            return $posts;
        } else {
            return [];
        }
    }
}
