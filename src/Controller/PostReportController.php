<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\Post;
use App\Entity\PostReport;
use App\Repository\PostReportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PostReportController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PostReportRepository
     */
    private $postReportRepository;

    /**
     * PostReportController constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     * @param PostReportRepository $postReportRepository
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        PostReportRepository $postReportRepository
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->postReportRepository = $postReportRepository;
    }

    /**
     * @param $postId
     * @return PostReport[]
     */
    public function getByPost($postId)
    {
        /** @var Post $post */
        $post = $this->entityManager
            ->getRepository(Post::class)
            ->find($postId);

        if ( !is_null($post) ) {
            /** @var PostReport[] $posts */
            $reports = $this->postReportRepository
                ->getPostReportsByPost($post);

            return $reports;
        } else {
            return [];
        }
    }

    /**
     * @param Request $request
     * @param $postId
     * @return array
     */
    public function addOrRemoveReport(Request $request, $postId)
    {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());

        /** @var Post $post */
        $post = $this->entityManager
            ->getRepository(Post::class)
            ->find($postId);

        $wantsToReport = (bool) $request->get('report');

        if ( !is_null($post) ) {
            /** @var Person $person */
            $person = $this->tokenStorage->getToken()->getUser();

            $reports = $this->postReportRepository->getPostReportsByPersonAndPost($person, $post);

            if ( $wantsToReport ) {
                if ( count($reports) <= 0 ) {
                    $newReport = new PostReport();
                    $newReport
                        ->setPost($post)
                        ->setPerson($person);
                    $this->entityManager->persist($newReport);
                    $this->entityManager->flush();
                }
            } else {
                if ( count($reports) > 0 ) {
                    /** @var PostReport $oldReport */
                    foreach ( $reports as $oldReport ) {
                        $this->entityManager->remove($oldReport);
                    }
                    $this->entityManager->flush();
                }
            }

            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}
