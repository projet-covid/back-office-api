<?php

namespace App\Repository;

use App\Entity\Facility;
use App\Entity\Person;
use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use Doctrine\ORM\Query\QueryException;
use Symfony\Component\Security\Core\Security;

class PostRepository extends ServiceEntityRepository
{
    const ITEMS_PER_PAGE = 20;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var Security */
    private $security;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        parent::__construct($registry, Post::class);
    }

    private function applyCurrentUserRules(QueryBuilder $qb)
    {
        /** @var Person $user */
        $user = $this->security->getUser();

        if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
            if ( $this->security->isGranted('ROLE_ADMIN') ) {
                $qb->andWhere($qb->expr()->eq('f.group', ':group'));
                $qb->setParameter('group', $user->getGroup());
            } else {
                $qb->andWhere($qb->expr()->in('f', ':facilities'));
                $qb->setParameter('facilities', $user->getFacilities());
            }
        }
    }

    /**
     * @param Facility $facility
     * @param int $page
     * @return Paginator
     * @throws QueryException
     */
    public function getPostsByFacility(Facility $facility, ?int $page = 1)
    {
        $firstResult = ($page -1) * self::ITEMS_PER_PAGE;

        $qb = $this->createQueryBuilder('p');
        $qb->innerJoin('p.facilities', 'f')
            ->andWhere($qb->expr()->in('f.id', $facility->getId()))
            ->andWhere($qb->expr()->eq('p.archived', ':archived'))
            ->addOrderBy('p.id', 'DESC')
            ->setParameter('archived', false, \PDO::PARAM_BOOL);

        $this->applyCurrentUserRules($qb);

        if ( $page != null ) {
            $criteria = Criteria::create()
                ->setFirstResult($firstResult)
                ->setMaxResults(self::ITEMS_PER_PAGE);
            $qb->addCriteria($criteria);

            $doctrinePaginator = new DoctrinePaginator($qb);
            return new Paginator($doctrinePaginator);
        } else {
            return $qb->getQuery()->getResult();
        }
    }
}
