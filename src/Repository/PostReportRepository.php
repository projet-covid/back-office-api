<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Post;
use App\Entity\PostReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class PostReportRepository extends ServiceEntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var Security */
    private $security;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        parent::__construct($registry, PostReport::class);
    }

    private function applyCurrentUserRules(QueryBuilder $qb)
    {
        /** @var Person $user */
        $user = $this->security->getUser();

        if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
            if ( $this->security->isGranted('ROLE_ADMIN') ) {
                $qb->innerJoin('pr.post', 'prp');
                $qb->innerJoin('prp.facilities', 'prpf');
                $qb->innerJoin('prpf.group', 'prpfg');
                $qb->andWhere($qb->expr()->eq('prpfg', ':group'));
                $qb->setParameter('group', $user->getGroup());
            } else {
                $qb->innerJoin('pr.post', 'prp');
                $qb->innerJoin('prp.facilities', 'prpf');
                $qb->andWhere($qb->expr()->in('prpf', ':facilities'));
                $qb->setParameter('facilities', $user->getFacilities());
            }
        }
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function getPostReportsByPost(Post $post)
    {
        $qb = $this->createQueryBuilder('pr');
        $qb
            ->where($qb->expr()->eq('pr.post', ':post'))
            ->orderBy('pr.id', 'DESC')
            ->setParameter('post', $post);

        $this->applyCurrentUserRules($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Person $person
     * @param Post $post
     * @return mixed
     */
    public function getPostReportsByPersonAndPost(Person $person, Post $post)
    {
        $qb = $this->createQueryBuilder('pr');
        $qb
            ->innerJoin('pr.post', 'pr_post')
            ->innerJoin('pr.person', 'pr_person')
            ->where($qb->expr()->eq('pr_post.id', $post->getId()))
            ->andWhere($qb->expr()->eq('pr_person.id', $person->getId()))
            ->orderBy('pr.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}
