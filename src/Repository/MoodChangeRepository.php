<?php

namespace App\Repository;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use App\Entity\MoodChange;
use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\Query\QueryException;
use Symfony\Component\Security\Core\Security;

class MoodChangeRepository extends ServiceEntityRepository
{
    const ITEMS_PER_PAGE = 20;

    /** @var TokenStorageInterface  */
    private $tokenStorage;

    /** @var Security */
    private $security;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        parent::__construct($registry, MoodChange::class);
    }

    private function applyCurrentUserRules(QueryBuilder $qb)
    {
        $user = $this->security->getUser();

        if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
            if ( $this->security->isGranted('ROLE_ADMIN') ) {
                $qb->innerJoin('p.group', 'g');
                $qb->andWhere($qb->expr()->eq('g', ':group'));
                $qb->setParameter('group', $user->getGroup());
            } else {
                $qb->andWhere($qb->expr()->eq('p', ':user'));
                $qb->setParameter('user', $user);
            }
        }
    }

    /**
     * @param Person $person
     * @param int $page
     * @return Paginator
     * @throws QueryException
     */
    public function getMoodChangesByPerson(Person $person, int $page = 1)
    {
        $firstResult = ($page -1) * self::ITEMS_PER_PAGE;

        $qb = $this->createQueryBuilder('mc');
        $qb->innerJoin('mc.person', 'p')
            ->innerJoin('mc.mood', 'm')
            ->where($qb->expr()->eq('p.id', ':person_id'))
            ->orderBy('mc.createdAt', 'DESC')
            ->setParameter('person_id', $person->getId());

        $this->applyCurrentUserRules($qb);

        $criteria = Criteria::create()
            ->setFirstResult($firstResult)
            ->setMaxResults(self::ITEMS_PER_PAGE);

        $qb->addCriteria($criteria);

        $doctrinePaginator = new DoctrinePaginator($qb);
        return new Paginator($doctrinePaginator);
    }

    /**
     * @param Person $person
     * @return Paginator
     */
    public function getLastMoodChangeByPerson(Person $person)
    {
        $qb = $this->createQueryBuilder('mc');
        $qb->innerJoin('mc.person', 'p')
            ->innerJoin('mc.mood', 'm')
            ->where($qb->expr()->eq('p.id', $person->getId()))
            ->orderBy('mc.createdAt', 'DESC')
            ->setMaxResults(1);

        $this->applyCurrentUserRules($qb);

        return $qb->getQuery()->getResult();
    }
}
