<?php

namespace App\Repository;

use App\Entity\Person;
use App\Entity\Post;
use App\Entity\PostLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class PostLikeRepository extends ServiceEntityRepository
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var Security */
    private $security;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        Security $security
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->security = $security;
        parent::__construct($registry, PostLike::class);
    }

    private function applyCurrentUserRules(QueryBuilder $qb)
    {
        /** @var Person $user */
        $user = $this->security->getUser();

        if ( $user != null && !$this->security->isGranted('ROLE_SUPER_ADMIN') ) {
            if ( $this->security->isGranted('ROLE_ADMIN') ) {
                $qb->innerJoin('pl.post', 'plp');
                $qb->innerJoin('plp.facilities', 'plpf');
                $qb->innerJoin('plpf.group', 'plpfg');
                $qb->andWhere($qb->expr()->eq('plpfg', ':group'));
                $qb->setParameter('group', $user->getGroup());
            } else {
                $qb->innerJoin('pl.post', 'plp');
                $qb->innerJoin('plp.facilities', 'plpf');
                $qb->andWhere($qb->expr()->in('plpf', ':facilities'));
                $qb->setParameter('facilities', $user->getFacilities());
            }
        }
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function getPostLikesByPost(Post $post)
    {
        $qb = $this->createQueryBuilder('pl');
        $qb
            ->where($qb->expr()->eq('pl.post', ':post'))
            ->orderBy('pl.id', 'DESC')
            ->setParameter('post', $post);

        $this->applyCurrentUserRules($qb);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Person $person
     * @param Post $post
     * @return mixed
     */
    public function getPostLikesByPersonAndPost(Person $person, Post $post)
    {
        $qb = $this->createQueryBuilder('pl');
        $qb
            ->innerJoin('pl.post', 'pl_post')
            ->innerJoin('pl.person', 'pl_person')
            ->where($qb->expr()->eq('pl_post.id', $post->getId()))
            ->andWhere($qb->expr()->eq('pl_person.id', $person->getId()))
            ->orderBy('pl.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}
