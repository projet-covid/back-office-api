<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403195325 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE person_facility (person_id INT NOT NULL, facility_id INT NOT NULL, PRIMARY KEY(person_id, facility_id))');
        $this->addSql('CREATE INDEX IDX_841F3185217BBB47 ON person_facility (person_id)');
        $this->addSql('CREATE INDEX IDX_841F3185A7014910 ON person_facility (facility_id)');
        $this->addSql('ALTER TABLE person_facility ADD CONSTRAINT FK_841F3185217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person_facility ADD CONSTRAINT FK_841F3185A7014910 FOREIGN KEY (facility_id) REFERENCES facility (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE person_facility');
    }
}
