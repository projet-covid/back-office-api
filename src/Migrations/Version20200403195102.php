<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403195102 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE facility_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE grouping_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE media_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE mood_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE mood_change_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE person_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_like_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_report_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE facility (id INT NOT NULL, group_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_105994B2FE54D947 ON facility (group_id)');
        $this->addSql('CREATE TABLE grouping (id INT NOT NULL, owner_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8FA87187E3C61F9 ON grouping (owner_id)');
        $this->addSql('CREATE TABLE media (id INT NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE mood (id INT NOT NULL, label VARCHAR(255) NOT NULL, icon VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE mood_change (id INT NOT NULL, mood_id INT DEFAULT NULL, person_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_235813C7B889D33E ON mood_change (mood_id)');
        $this->addSql('CREATE INDEX IDX_235813C7217BBB47 ON mood_change (person_id)');
        $this->addSql('CREATE TABLE person (id INT NOT NULL, role_id INT DEFAULT NULL, group_id INT DEFAULT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_logged TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, archived BOOLEAN DEFAULT NULL, show_mood BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_34DCD176D60322AC ON person (role_id)');
        $this->addSql('CREATE INDEX IDX_34DCD176FE54D947 ON person (group_id)');
        $this->addSql('CREATE TABLE post (id INT NOT NULL, mood_id INT DEFAULT NULL, person_id INT DEFAULT NULL, media_id INT DEFAULT NULL, content VARCHAR(255) NOT NULL, archived BOOLEAN DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5A8A6C8DB889D33E ON post (mood_id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8D217BBB47 ON post (person_id)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8DEA9FDD75 ON post (media_id)');
        $this->addSql('CREATE TABLE post_facility (post_id INT NOT NULL, facility_id INT NOT NULL, PRIMARY KEY(post_id, facility_id))');
        $this->addSql('CREATE INDEX IDX_AFB41D134B89032C ON post_facility (post_id)');
        $this->addSql('CREATE INDEX IDX_AFB41D13A7014910 ON post_facility (facility_id)');
        $this->addSql('CREATE TABLE post_like (id INT NOT NULL, person_id INT DEFAULT NULL, post_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_653627B8217BBB47 ON post_like (person_id)');
        $this->addSql('CREATE INDEX IDX_653627B84B89032C ON post_like (post_id)');
        $this->addSql('CREATE TABLE post_report (id INT NOT NULL, person_id INT DEFAULT NULL, post_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F40D93E1217BBB47 ON post_report (person_id)');
        $this->addSql('CREATE INDEX IDX_F40D93E14B89032C ON post_report (post_id)');
        $this->addSql('CREATE TABLE role (id INT NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE facility ADD CONSTRAINT FK_105994B2FE54D947 FOREIGN KEY (group_id) REFERENCES grouping (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE grouping ADD CONSTRAINT FK_8FA87187E3C61F9 FOREIGN KEY (owner_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mood_change ADD CONSTRAINT FK_235813C7B889D33E FOREIGN KEY (mood_id) REFERENCES mood (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mood_change ADD CONSTRAINT FK_235813C7217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176D60322AC FOREIGN KEY (role_id) REFERENCES role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176FE54D947 FOREIGN KEY (group_id) REFERENCES grouping (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DB889D33E FOREIGN KEY (mood_id) REFERENCES mood (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8D217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post ADD CONSTRAINT FK_5A8A6C8DEA9FDD75 FOREIGN KEY (media_id) REFERENCES media (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_facility ADD CONSTRAINT FK_AFB41D134B89032C FOREIGN KEY (post_id) REFERENCES post (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_facility ADD CONSTRAINT FK_AFB41D13A7014910 FOREIGN KEY (facility_id) REFERENCES facility (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_like ADD CONSTRAINT FK_653627B8217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_like ADD CONSTRAINT FK_653627B84B89032C FOREIGN KEY (post_id) REFERENCES post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_report ADD CONSTRAINT FK_F40D93E1217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_report ADD CONSTRAINT FK_F40D93E14B89032C FOREIGN KEY (post_id) REFERENCES post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE post_facility DROP CONSTRAINT FK_AFB41D13A7014910');
        $this->addSql('ALTER TABLE facility DROP CONSTRAINT FK_105994B2FE54D947');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD176FE54D947');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8DEA9FDD75');
        $this->addSql('ALTER TABLE mood_change DROP CONSTRAINT FK_235813C7B889D33E');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8DB889D33E');
        $this->addSql('ALTER TABLE grouping DROP CONSTRAINT FK_8FA87187E3C61F9');
        $this->addSql('ALTER TABLE mood_change DROP CONSTRAINT FK_235813C7217BBB47');
        $this->addSql('ALTER TABLE post DROP CONSTRAINT FK_5A8A6C8D217BBB47');
        $this->addSql('ALTER TABLE post_like DROP CONSTRAINT FK_653627B8217BBB47');
        $this->addSql('ALTER TABLE post_report DROP CONSTRAINT FK_F40D93E1217BBB47');
        $this->addSql('ALTER TABLE post_facility DROP CONSTRAINT FK_AFB41D134B89032C');
        $this->addSql('ALTER TABLE post_like DROP CONSTRAINT FK_653627B84B89032C');
        $this->addSql('ALTER TABLE post_report DROP CONSTRAINT FK_F40D93E14B89032C');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD176D60322AC');
        $this->addSql('DROP SEQUENCE facility_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE grouping_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE media_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE mood_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE mood_change_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE person_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_like_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_report_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE role_id_seq CASCADE');
        $this->addSql('DROP TABLE facility');
        $this->addSql('DROP TABLE grouping');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE mood');
        $this->addSql('DROP TABLE mood_change');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE post');
        $this->addSql('DROP TABLE post_facility');
        $this->addSql('DROP TABLE post_like');
        $this->addSql('DROP TABLE post_report');
        $this->addSql('DROP TABLE role');
    }
}
