<?php

namespace App\EventListener;

use App\Entity\Person;
use Cassandra\Exception\UnauthorizedException;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * @param RequestStack $requestStack
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager
    )
    {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @param JWTCreatedEvent $event
     *
     * @return void
     * @throws \Exception
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var Person $user */
        $user = $this->tokenStorage->getToken()->getUser();

        if ( $user->getLastLogged() !== null ) {
            $user->setLastLogged(new \DateTime());
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $expiration = new \DateTime('+1 year');
        $expiration->setTime(2, 0, 0);

        $payload = $event->getData();
        $payload['lastLogged'] = $user->getLastLogged();
        $payload['user_id'] = $this->tokenStorage->getToken()->getUser()->getId();
        if ( !is_null($this->tokenStorage->getToken()->getUser()->getGroup()) ) {
            $payload['user_group_id'] = $this->tokenStorage->getToken()->getUser()->getGroup()->getId();
            $payload['user_group_label'] = $this->tokenStorage->getToken()->getUser()->getGroup()->getLabel();
        }
        else {
            $payload['user_facilities'] = [];
            foreach ( $user->getFacilities() as $facility ) {
                $payload['user_facilities'][] = [
                    'id' => $facility->getId(),
                    'label' => $facility->getLabel()
                ];
            }
        }
        $payload['permissions']  = $this->tokenStorage->getToken()->getUser()->getRoles();
        $payload['exp'] = $expiration->getTimestamp();

        $event->setData($payload);
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $event->stopPropagation();
        /** @var Person $user */
        $user = $this->tokenStorage->getToken()->getUser();
        if ( $user->isArchived() ) {
            $event->setData(['message' => 'Votre compte est verrouillé.']);
        }
    }
}
