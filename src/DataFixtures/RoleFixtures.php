<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $superAdminRole = new Role();
        $superAdminRole->setLabel('ROLE_SUPER_ADMIN');
        $superAdminRole->setDisplayedLabel('Super Administrateur');

        $adminRole = new Role();
        $adminRole->setLabel('ROLE_ADMIN');
        $adminRole->setDisplayedLabel('Administrateur');

        $userRole = new Role();
        $userRole->setLabel('ROLE_USER');
        $userRole->setDisplayedLabel('Utilisateur');

        $manager->persist($superAdminRole);
        $manager->persist($adminRole);
        $manager->persist($userRole);

        $manager->flush();

        $this->addReference('role.super_admin', $superAdminRole);
        $this->addReference('role.admin', $adminRole);
        $this->addReference('role.user', $userRole);
    }
}
