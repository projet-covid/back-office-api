<?php

namespace App\DataFixtures;

use App\Entity\Mood;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MoodFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $mood = new Mood();
        $mood->setLabel('Pleine forme');
        $mood->setIcon('pleineForme');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Motivé');
        $mood->setIcon('motive');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Serein');
        $mood->setIcon('serein');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Triste');
        $mood->setIcon('triste');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Fatigué');
        $mood->setIcon('fatigue');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Epuisé');
        $mood->setIcon('epuise');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Aidant');
        $mood->setIcon('aidant');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('Stressé');
        $mood->setIcon('stresse');

        $manager->persist($mood);

        $mood = new Mood();
        $mood->setLabel('En colère');
        $mood->setIcon('encolere');

        $manager->persist($mood);

        $manager->flush();
    }
}
