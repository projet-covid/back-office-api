<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Post;
use App\Entity\Role;
use App\Repository\MoodChangeRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var MoodChangeRepository
     */
    private $mcRepo;

    public function __construct(MoodChangeRepository $mcRepo)
    {
        $this->mcRepo = $mcRepo;
    }

    public function load(ObjectManager $manager)
    {
        $role = $manager->getRepository(Role::class)->findOneBy(['label' => 'ROLE_USER']);
        $users = $manager->getRepository(Person::class)->findBy(['role' => $role]);

        foreach ( $users as $user ) {
            for ( $i = 0; $i < rand(1, 10); $i++ ) {
                $post = new Post();
                $int= mt_rand(1577840400,1586196000);
                $post->setPublishedAt((new \DateTime())->setTimestamp($int));
                $post->setContent('Jelly beans sweet cupcake soufflé toffee chupa chups dessert danish jelly-o. Pudding jelly brownie. Candy canes pastry chocolate bar danish gummies gummies. Jelly-o liquorice jelly-o soufflé cookie apple pie jelly. Cupcake jelly liquorice gummies marzipan. Ice cream bonbon toffee.');
                $post->setPerson($user);
                $post->setMediaUrl('https://picsum.photos/200/300?random=1');
                $shareMood = rand(0, 1);
                if ( $shareMood == 1 ) {
                    $moodChange = $this->mcRepo->getLastMoodChangeByPerson($user);
                    if ( is_countable($moodChange) ) {
                        $post->setMood($moodChange[0]->getMood());
                    } elseif ( !is_null($moodChange) ) {
                        $post->setMood($moodChange->getMood());
                    }
                }
                foreach ($user->getFacilities() as $facility) {
                    $post->addFacility($facility);
                }
                $manager->persist($post);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            MoodChangeFixtures::class
        ];
    }
}
