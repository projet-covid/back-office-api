<?php

namespace App\DataFixtures;

use App\Entity\Facility;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FacilityFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $facilities = [];

        for ( $i = 0; $i < 10; $i++ ) {
            $group = $this->getReference('group.Groupe' . $i);

            for ( $j = 0; $j < 10; $j++ ) {
                $facility = new Facility();
                $facility->setLabel('Etablissement' . $i . $j);
                $facility->setGroup($group);

                $manager->persist($facility);

                $this->addReference('facility.' . $facility->getLabel(), $facility);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            GroupFixtures::class
        ];
    }
}
