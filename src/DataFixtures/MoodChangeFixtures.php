<?php

namespace App\DataFixtures;

use App\Entity\Mood;
use App\Entity\MoodChange;
use App\Entity\Person;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MoodChangeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $role = $manager->getRepository(Role::class)->findOneBy(['label' => 'ROLE_USER']);
        $users = $manager->getRepository(Person::class)->findBy(['role' => $role]);
        $moods = $manager->getRepository(Mood::class)->findAll();

        foreach ($users as $user) {
            for ( $i = 0; $i < rand(1, 20); $i++ ) {
                $int= mt_rand(1577840400,1586196000);
                $mc = new MoodChange();
                $mood = $moods[rand(0, count($moods) - 1)];
                $mc->setPerson($user);
                $mc->setCreatedAt((new \DateTime())->setTimestamp($int));
                $mc->setMood($mood);
                $manager->persist($mc);
            }
            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            MoodFixtures::class
        ];
    }
}
