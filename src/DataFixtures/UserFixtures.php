<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ( $i = 0; $i < 10; $i++ ) {
            for ( $j = 0; $j < 10; $j++ ) {
                for ( $k = 0; $k < 10; $k++ ) {
                    $user = new Person();
                    $user->setLastName('NOM_' . $i . $j . $k);
                    $user->setFirstName('PRENOM_' . $i . $j . $k);
                    $user->setUsername('user' . $i . $j . $k);
                    $user->setPassword('$2y$12$zmoGjCnLHUkscAFGj1FflOLX/BYF0Bw6VrF8tIDHBEi6VsfDZUhCK');
                    $user->setRole($this->getReference('role.user'));
                    $user->setGroup($this->getReference('group.Groupe' . $i));
                    for ( $l = 0; $l < rand(1, 9); $l++ ) {
                        $user->addFacility($this->getReference('facility.Etablissement' . $i . $l));
                    }

                    $manager->persist($user);

                    $this->addReference('person.user_' . $i . $j . $k, $user);
                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RoleFixtures::class,
            GroupFixtures::class,
            FacilityFixtures::class
        ];
    }
}
