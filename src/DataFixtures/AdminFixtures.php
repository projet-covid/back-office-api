<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AdminFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $superAdmin = new Person();
        $superAdmin->setLastName('SUPER');
        $superAdmin->setFirstName('ADMIN');
        $superAdmin->setUsername('superadmin');
        $superAdmin->setPassword('$2y$12$zmoGjCnLHUkscAFGj1FflOLX/BYF0Bw6VrF8tIDHBEi6VsfDZUhCK');
        $superAdmin->setRole($this->getReference('role.super_admin'));

        $manager->persist($superAdmin);

        $admins = [];

        for ( $i = 0; $i < 10; $i++ ) {
            $admin = new Person();
            $admin->setLastName('ADMIN' . $i);
            $admin->setFirstName('ADMIN' . $i);
            $admin->setUsername('simpleadmin' . $i);
            $admin->setPassword('$2y$12$zmoGjCnLHUkscAFGj1FflOLX/BYF0Bw6VrF8tIDHBEi6VsfDZUhCK');
            $admin->setRole($this->getReference('role.admin'));
            $manager->persist($admin);
            $admins[] = $admin;
        }

        $manager->flush();

        $this->addReference('person.superadmin', $superAdmin);

        foreach ($admins as $admin) {
            $this->addReference('person.' . $admin->getUsername(), $admin);
        }
    }

    public function getDependencies()
    {
        return [
            RoleFixtures::class
        ];
    }
}
