<?php

namespace App\DataFixtures;

use App\Entity\Group;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GroupFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $groups = [];

        for ( $i = 0; $i < 10; $i++ ) {
            $group = new Group();
            $group->setLabel('Groupe' . $i);
            $owner = $this->getReference('person.simpleadmin' . $i);
            $group->setOwner($owner);
            $owner->setGroup($group);

            $manager->persist($group);
            $manager->persist($owner);

            $this->addReference('group.' . $group->getLabel(), $group);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AdminFixtures::class
        ];
    }
}
