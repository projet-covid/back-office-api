<?php

namespace App\Validator;

use App\Entity\Facility;
use App\Entity\MoodChange;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final class AdminGroupsGenerator
{
    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    public function __invoke($entity): array
    {
        switch (get_class($entity)) {
            case Facility::class:
                return $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN', $entity) ?
                    [
                        'postValidationSuperAdmin',
                        'putValidationSuperAdmin'
                    ] :
                    [
                        'postValidationAdmin',
                        'putValidationAdmin'
                    ];
            case MoodChange::class:
                return $this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN', $entity) ?
                    [
                        'postValidation'
                    ] :
                    [
                        'Default'
                    ];
        }
        return ['Default'];
    }
}
